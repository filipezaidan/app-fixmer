import React from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import { Container, Button, Icon, Header, Left, Right, Body, Title, View, Fab, List, ListItem, Thumbnail, Text, Badge, Content, Tab, Tabs, TabHeading, Card, CardItem, Item, Label, Input, Form, Image } from 'native-base';


export default class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = { loading: true };
  }

  async componentWillMount() {
    await Expo.Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
      Ionicons: require("@expo/vector-icons/fonts/Ionicons.ttf"),
    });
    this.setState({ loading: false });
  }
  
  render() {
    if (this.state.loading) {
      return <Expo.AppLoading />;
    }
    return (
      <Container style={styles.container}> 
        <Thumbnail 
        square large
        scaleX={3} scaleY={3} 
        style={{margin: 30}}
        source={require('./img/logo.png')}
        />
        <Text style={styles.text}>FIXme</Text>
        

        <Button style={styles.button} onPress={() => this.props.navigation.navigate('tela2')} iconRight light >
            <Text>Next</Text>
            <Icon name='arrow-forward' />
          </Button>
      </Container>
     
    );
  }
}

const styles = StyleSheet.create({
  container:{
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#222f3e',
    alignItems: 'center',

  },

  text:{
    color: '#fff',
    marginTop: 10,
    textAlign : 'center',
    opacity: 0.9,
    fontSize: 40,
    marginTop: 50,
  },

  logo:{
    width: 100,
    height:100,

  },

  button: {
    padding: 20,
    paddingHorizontal: 10,
    justifyContent: 'center',
    marginTop: 70,
    marginLeft: 120,

  },

})