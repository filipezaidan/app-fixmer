import React from 'react';
import { StyleSheet, View} from 'react-native';
import Principal from './Principal';
import Login from './Login';
import Mapas from './Mapas';
import {createStackNavigator} from 'react-navigation';
import Categoria from './Categoria';

export default class App extends React.Component {
  render() {
    return (
      <View style={styles.container}>
       <Navegar/>
      </View>
    );
  }
}
const Navegar = createStackNavigator({
  tela1: {screen: Principal, 
  navigationOptions:{
    header:null
  }
},
  tela2: {screen: Login,
  navigationOptions:{
    headerTitle: 'Login'
  }
},
  tela3:{screen: Mapas,
    navigationOptions:{
      header:<Categoria/>
    }
  },

})
const styles = StyleSheet.create({
  container:{
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#008080',
 
  },

})