import React from 'react';
import { StyleSheet, View , Picker, Text} from 'react-native';


export default class Categoria extends React.Component {

    state = {categoria: ""}
    updateCategoria = (categoria) => {
        this.setState({categoria:categoria})
    }

  render() {
    return (
      <View style={styles.categoria}>
        <Picker selectedValue = {this.state.categoria} onValueChange = {this.updateCategoria}>
               <Picker.Item label = "Escolha um serviço" value = "serviço" />
               <Picker.Item label = "Eletrodomesticos" value = "eletrodomesticos" />
               <Picker.Item label = "Mecânico" value = "mecânico" />
               <Picker.Item label = "Eletricista" value = "eletrecista" />
            </Picker>
            <Text style = {styles.text}>{this.state.user}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
    text: {
        fontSize: 30,
        alignSelf: 'center',
        color: 'red'
     },
    
     categoria:{
         paddingTop:20,
     },
})