import React from 'react';
import { StyleSheet} from 'react-native';
import { Container, Header, Left, Right, Body, Title, Button, Icon, View, Fab, List, ListItem, Thumbnail, Text, Badge, Content, Tab, Tabs, TabHeading, Card, CardItem, Item, Label, Input, Form } from 'native-base';
import Expo from "expo";

export default class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = { loading: true };
  }

  async componentWillMount() {
    await Expo.Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
      Ionicons: require("@expo/vector-icons/fonts/Ionicons.ttf"),
    });
    this.setState({ loading: false });
  }
  
  render() {
    if (this.state.loading) {
      return <Expo.AppLoading />;
    }
    return (
      <Container style={styles.container}>
        <Form >
          <Item floatingLabel>

            <Label style={styles.label}>Email</Label>
              <Input
                  keyboardType='email-address'
                  autoCorrect = {false}
                  autoCapitalize = 'none'
                  placeholderTextColor = 'white'
              />
          </Item>

          <Item floatingLabel>

            <Label style={styles.label}>Password</Label>
              <Input
                  secureTextEntry={true}
                  placeholderTextColor='red'
                  autoCorrect = {false}
                  autoCapitalize = 'none'
                  placeholderTextColor = '#fff'
              />
          </Item>          
        </Form>
        <Button style={styles.button} onPress={() => this.props.navigation.navigate('tela3')}>
          <Text>Entrar</Text>
        </Button>
      </Container>
      
    );
  }
}


const styles = StyleSheet.create({
  container:{
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#222f3e'
  },

  button: {
    backgroundColor: 'red',
    justifyContent: 'center',
    marginTop: 70,
    marginLeft: 140,
  },
  label: {
    color: 'white'
  },

})